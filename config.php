<?php
	/**
	 * 
	 */
	class database
	{
		var $host = "localhost";
		var $username = "root";
		var $password = "";
		var $database = "sewa_buku";
		var $koneksi = "";
		
		function __construct()
		{
			$this ->koneksi = mysqli_connect($this->host, $this->username, $this->password, $this->database);
			if(mysqli_connect_errno()){
				echo "Koneksi database gagal : " .mysqli_connect_errno();
			} 
		}
		function tampil_data(){
			$data = mysqli_query($this->koneksi, "SELECT * from data_peminjam");
			while($row = mysqli_fetch_array($data)){
				$hasil[]= $row;
			}
			return $hasil;
		}
		function tambah_data_peminjam($kode_peminjam, $nama_peminjam, $jenis_kelamin, $tanggal_lahir, $alamat, $pekerjaan)
		{
    		$query = "INSERT INTO data_peminjam (kode_peminjam, nama_peminjam, jenis_kelamin, tanggal_lahir, alamat, pekerjaan) VALUES ('$kode_peminjam', '$nama_peminjam', '$jenis_kelamin', '$tanggal_lahir', '$alamat', '$pekerjaan')";
    		mysqli_query($this->koneksi, $query);
		}
		function kode_peminjam($id)
		{
		    $data_peminjam = mysqli_query($this->koneksi, "SELECT * FROM data_peminjam WHERE id = '$id'");
		    while ($row_peminjam = mysqli_fetch_assoc($data_peminjam)) {
		        $hasil_peminjam[] = $row_peminjam;
		    }
		    return $hasil_peminjam;
		}

		function edit_data_peminjam($id,$kode_peminjam, $nama_peminjam, $jenis_kelamin, $tanggal_lahir, $alamat, $pekerjaan)
		{
			mysqli_query($this->koneksi,"UPDATE data_peminjam set nama_peminjam = '$nama_peminjam', jenis_kelamin = '$jenis_kelamin', tanggal_lahir = '$tanggal_lahir', alamat = '$alamat', pekerjaan = '$pekerjaan' where id = '$id'");
		}
		function hapus_data_peminjam($id){
			mysqli_query($this->koneksi,"DELETE from data_peminjam where id = '$id'");
		}
		function tambah_data_jenis_buku($kode_jenis_buku,$nama_jenis_buku)
		{
			mysqli_query($this->koneksi,"INSERT INTO data_jenis_buku values ('','$kode_jenis_buku','$nama_jenis_buku')");
		}
		function tampil_data_jenis_buku()
		{
			$data = mysqli_query($this->koneksi,"SELECT * from data_jenis_buku");
			while($row = mysqli_fetch_array($data)){
				$hasil[] = $row;
			}
			return $hasil;
		}
		function ambil_data_buku()
		{
			$data_buku = mysqli_query($this->koneksi, "SELECT * FROM data_buku");
			while ($row_data_buku = mysqli_fetch_array($data_buku)) {
				$hasil_data_buku[] = $row_data_buku;
			}
			return $hasil_data_buku;
		}

		function tambah_data_penerbit($kode_penerbit,$nama_penerbit)
		{
			mysqli_query($this->koneksi,"insert into data_penerbit values (' ','$kode_penerbit','$nama_penerbit')");
		}
		function tampil_data_penerbit()
		{
			$data = mysqli_query($this->koneksi,"select * from data_penerbit");
			while($row = mysqli_fetch_array($data)){
				$hasil[] = $row;
			}
			return $hasil;
		}

		function tambah_data_pengarang($kode_pengarang,$nama_pengarang)
		{
		mysqli_query($this->koneksi,"insert into data_pengarang values ('','$kode_pengarang','$nama_pengarang')");
		}
		function tampil_data_pengarang()
		{
	
			$data = mysqli_query($this->koneksi,"select * from data_pengarang");
			while($row = mysqli_fetch_array($data)){
				$hasil[] = $row;
			}
			return $hasil;
		}
		
		function ambil_data_pengarang(){
			$data_pengarang = mysqli_query($this->koneksi, "select * from data_pengarang");
			while ($row_data_pengarang = mysqli_fetch_array($data_pengarang)) {
				$hasil_data_pengarang[] = $row_data_pengarang;
			}
	
			return $hasil_data_pengarang;
		}
	
		function ambil_data_jenis_buku(){
			$data_jenis_buku = mysqli_query($this->koneksi, "select * from data_jenis_buku");
			while ($row_data_jenis_buku = mysqli_fetch_array($data_jenis_buku)) {
				$hasil_data_jenis_buku[] = $row_data_jenis_buku;
			}
	
			return $hasil_data_jenis_buku;
		}
	
		function ambil_data_penerbit(){
			$data_penerbit = mysqli_query($this->koneksi, "select * from data_penerbit");
			while ($row_data_penerbit = mysqli_fetch_array($data_penerbit)) {
				$hasil_data_penerbit[] = $row_data_penerbit;
			}
	
			return $hasil_data_penerbit;
		}
	
		function tambah_data_buku($kode_buku,$judul_buku,$kode_pengarang,$kode_jenis_buku,$kode_penerbit,$isbn,$tahun,$deskripsi,$jumlah) 
		{
			mysqli_query($this->koneksi,"insert into data_buku values ('', '$kode_buku', '$judul_buku', '$kode_pengarang', '$kode_jenis_buku', '$kode_penerbit', '$isbn', '$tahun', '$deskripsi', '$jumlah')");
		}
		function tampil_data_buku()
		{
			$data = mysqli_query($this->koneksi,"select a.*, b.*, c.*, d.* from data_buku a
					INNER JOIN data_pengarang b ON b.kode_pengarang = a.kode_pengarang
					INNER JOIN data_jenis_buku c ON c.kode_jenis_buku = a.kode_jenis_buku
					INNER JOIN data_penerbit d ON d.kode_penerbit = a.kode_penerbit");
	
		while ($row = mysqli_fetch_array($data)) {
			$hasil[] = $row;
		}
		return $hasil;
		}
		
		function ambil_data_peminjam()
		{
			$data_peminjam = mysqli_query($this->koneksi, "SELECT * FROM data_peminjam");
			while ($row_data_peminjam = mysqli_fetch_array($data_peminjam)) {
				$hasil_data_peminjam[] = $row_data_peminjam;
			}
			return $hasil_data_peminjam;
		}
	
		function tambah_peminjaman($kode_buku, $kode_peminjam)
		{
			$tanggal_pinjam = date('Y-m-d');
			$tanggal_kembali = date('Y-m-d', time() + (60 * 60 * 24 * 7));
			mysqli_query($this->koneksi, "INSERT INTO peminjaman VALUES ('', '$kode_buku', '$kode_peminjam', '$tanggal_pinjam', '$tanggal_kembali', '1')");
		}
	
		function tampil_peminjaman()
		{
			$data = mysqli_query($this->koneksi, "SELECT a.*, b.*, c.* FROM peminjaman a
				INNER JOIN data_buku b ON b.kode_buku = a.kode_buku
				INNER JOIN data_peminjam c ON c.kode_peminjam = a.kode_peminjam");
	
			while ($row = mysqli_fetch_array($data)) {
				$hasil[] = $row;
			}
			return $hasil;
		}
	}
?>